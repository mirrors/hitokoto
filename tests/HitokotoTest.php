<?php

/*
 * This file is part of the hui-ho/hitokoto.
 *
 * (c) hui-ho <hui-ho@outlook.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace HuiHo\Hitokoto\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use HuiHo\Hitokoto\Hitokoto;
use HuiHo\Hitokoto\Exceptions\HttpException;
use HuiHo\Hitokoto\Exceptions\InvalidArgumentException;
use Mockery\Matcher\AnyArgs;
use PHPUnit\Framework\TestCase;

class HitokotoTest extends TestCase
{
    public function testGet()
    {
        // 创建模拟接口响应值。
        $response = new Response(200, [], '{
            "id": 443,
            "hitokoto": "我和我的小伙伴们都惊呆了！",
            "type": "f",
            "from": "我和我的小伙伴们都惊呆了",
            "creator": "王世强",
            "created_at": "1468949629"
          }');

        // 创建模拟 http client。
        $client = \Mockery::mock(Client::class);

        // 指定将会产生的形为（在后续的测试中将会按下面的参数来调用）。
        $client->allows()->get('https://v1.hitokoto.cn', [
            'query' => [
                'encode' => 'json',
                'charset' => 'utf-8',
            ],
        ])->andReturn($response);

        // 将 `getHttpClient` 方法替换为上面创建的 http client 为返回值的模拟方法。
        $h = \Mockery::mock(Hitokoto::class)->makePartial();
        $h->allows()->getHttpClient()->andReturn($client);

        // 然后调用 `get` 方法，并断言返回值为模拟的返回值。
        $this->assertSame('我和我的小伙伴们都惊呆了！', $h->get()->content);
    }

    public function testGetWithGuzzleRuntimeException()
    {
        $client = \Mockery::mock(Client::class);
        $client->allows()
            ->get(new AnyArgs()) // 由于上面的用例已经验证过参数传递，所以这里就不关心参数了。
            ->andThrow(new \Exception('request timeout')); // 当调用 get 方法时会抛出异常。

        $h = \Mockery::mock(Hitokoto::class)->makePartial();
        $h->allows()->getHttpClient()->andReturn($client);

        // 接着需要断言调用时会产生异常。
        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('request timeout');

        $h->get();
    }

    public function testGetHttpClient()
    {
        $h = new Hitokoto();

        // 断言返回结果为 GuzzleHttp\ClientInterface 实例
        $this->assertInstanceOf(ClientInterface::class, $h->getHttpClient());
    }

    public function testSetGuzzleOptions()
    {
        $h = new Hitokoto();

        // 设置参数前，timeout 为 null
        $this->assertNull($h->getHttpClient()->getConfig('timeout'));

        // 设置参数
        $h->setGuzzleOptions(['timeout' => 5000]);

        // 设置参数后，timeout 为 5000
        $this->assertSame(5000, $h->getHttpClient()->getConfig('timeout'));
    }

    public function testType()
    {
        $h = new Hitokoto();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid type param: WEB');

        $h->type('WEB');
    }
}
