<?php

/*
 * This file is part of the hui-ho/hitokoto.
 *
 * (c) hui-ho <hui-ho@outlook.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace HuiHo\Hitokoto;

use GuzzleHttp\Psr7\Response;

class Sentence
{
    /**
     * ID，可以链接到 https://hitokoto.cn?id=[id] 查看这个一言的完整信息。
     *
     * @var int
     */
    public $id;

    /**
     * 一言正文.
     *
     * @var string
     */
    public $content;

    /**
     * 类型：Anime（动画）、Comic（漫画）、Game（游戏）、Nove（小说）、
     *      Myself（原创）、Internet（来自网络）、Other（其他）.
     *
     * @var string
     */
    public $type;

    /**
     * 一言的作者.
     *
     * @var string
     */
    public $from;

    /**
     * 添加者.
     *
     * @var string
     */
    public $creator;

    /**
     * 添加时间.
     *
     * @var string
     */
    public $created_at;

    /**
     * 初始化句子.
     *
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $content = json_decode($response->getBody()->getContents(), true);

        $this->id = isset($content['id']) ? (int) $content['id'] : 0;
        $this->content = isset($content['hitokoto']) ? $content['hitokoto'] : 'unknown';
        $this->from = isset($content['from']) ? $content['from'] : 'unknown';
        $this->creator = isset($content['creator']) ? $content['creator'] : 'unknown';
        $this->created_at = isset($content['created_at']) ? $content['created_at'] : time();

        if (isset($content['type']) && in_array($content['type'], ['a', 'b', 'c', 'd', 'e', 'f', 'g'])) {
            $types = [
                'a' => 'Anime',
                'b' => 'Comic',
                'c' => 'Game',
                'd' => 'Nove',
                'e' => 'Myself',
                'f' => 'Internet',
                'g' => 'Other',
            ];

            $this->type = $types[$content['type']];
        } else {
            $this->type = 'Other';
        }
    }

    /**
     * 以字符串的形式直接输出对象
     *
     * @return string
     */
    public function __toString()
    {
        return '「'.$this->content.'」 ———— '.$this->from;
    }
}
