<?php

/*
 * This file is part of the hui-ho/hitokoto.
 *
 * (c) hui-ho <hui-ho@outlook.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace HuiHo\Hitokoto\Exceptions;

class InvalidArgumentException extends Exception
{
}
