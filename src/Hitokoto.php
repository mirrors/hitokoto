<?php

/*
 * This file is part of the hui-ho/hitokoto.
 *
 * (c) hui-ho <hui-ho@outlook.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace HuiHo\Hitokoto;

use GuzzleHttp\Client;
use HuiHo\Hitokoto\Exceptions\HttpException;
use HuiHo\Hitokoto\Exceptions\InvalidArgumentException;

class Hitokoto
{
    protected $guzzleOptions = [];

    protected $queryOptions = [];

    public function getHttpClient()
    {
        return new Client($this->guzzleOptions);
    }

    public function setGuzzleOptions(array $options)
    {
        $this->guzzleOptions = $options;
    }

    public function get()
    {
        $url = 'https://v1.hitokoto.cn';

        $query = array_merge([
            'encode' => 'json',
            'charset' => 'utf-8',
        ], $this->queryOptions);

        array_filter([
            'encode' => 'json',
            'charset' => 'utf-8',
        ]);

        try {
            $response = $this->getHttpClient()->get($url, [
                'query' => $query,
            ]);

            return new Sentence($response);
        } catch (\Exception $e) {
            throw new HttpException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function type($type)
    {
        $types = [
            'Anime' => 'a',
            'Comic' => 'b',
            'Game' => 'c',
            'Novel' => 'd',
            'Myself' => 'e',
            'Internet' => 'f',
            'Other' => 'g',
        ];

        if (!in_array($type, array_keys($types))) {
            throw new InvalidArgumentException('Invalid type param: '.$type);
        }

        $this->queryOptions['c'] = $types[$type];

        return $this;
    }
}
