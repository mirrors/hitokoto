<?php

/*
 * This file is part of the hui-ho/hitokoto.
 *
 * (c) hui-ho <hui-ho@outlook.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace HuiHo\Hitokoto;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton(Hitokoto::class, function () {
            return new Hitokoto();
        });

        $this->app->alias(Hitokoto::class, 'hitokoto');
    }

    public function provides()
    {
        return [Hitokoto::class, 'hitokoto'];
    }
}
