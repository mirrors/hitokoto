# Hitokoto

[![Build Status](https://travis-ci.org/hui-ho/hitokoto.svg?branch=master)](https://travis-ci.org/hui-ho/hitokoto)
![StyleCI build status](https://github.styleci.io/repos/171098155/shield)

基于[一言网](Hitokoto.cn)的一言组件。

Hitokoto·一言是什么？官方的自我介绍如下：

> 一言网(Hitokoto.cn)创立于2016年，隶属于萌创Team，目前网站主要提供一句话服务。
>
> 动漫也好、小说也好、网络也好，不论在哪里，我们总会看到有那么一两个句子能穿透你的心。我们把这些句子汇聚起来，形成一言网络，以传递更多的感动。如果可以，我们希望我们没有停止服务的那一天。
>
> 简单来说，一言指的就是一句话，可以是动漫中的台词，也可以是网络上的各种小段子。
或是感动，或是开心，有或是单纯的回忆。来到这里，留下你所喜欢的那一句句话，与大家分享，这就是一言存在的目的。

## 安装

```shell
$ composer require huiho/hitokoto -vvv
```

## 使用

```php
use HuiHo\Hitokoto\Hitokoto;

$hitokoto = new Hitokoto;
```

### 直接输出

```php
echo $hitokoto->get();
```

示例:

```
「真相只有一个！」 ———— 柯南
```

### 详细数据

```php
$sentence = $hitokoto->get();
```

示例：

```
HuiHo\Hitokoto\Sentence Object
(
    [id] => 320
    [content] => 树大必有枯枝，人多必有白痴。
    [type] => Nove
    [from] => 谢谢你！坏运
    [creator] => hsk
    [created_at] => 1468949196
)
```

获取数据：

```php
$sentence->id; # ID
$sentence->content # 正文
$sentence->type # 分类
$sentence->from # 出处
$sentence->creator # 添加者
$sentence->created_at # 添加时间
```

### 按分类获取数据

```php
$hitokoto->type('Game')->get();
```

## 在 Laravel 中使用

在 Laravel 中使用也是同样的安装方式。

### 方法参数注入

```php
    .
    .
    .
    public function edit(Hitokoto $hitokoto) 
    {
        $response = $hitokoto->get();
    }
    .
    .
    .
```

### 服务名访问

```php
    .
    .
    .
    public function edit() 
    {
        $response = app('hitokoto')->get();
    }
    .
    .
    .
```

## 参考

- [一言 API 使用说明](https://hitokoto.cn/api)

## License

MIT